const game = () => {
  let pScore = 0;
  let cScore = 0;

  //Start the Game
  const startGame = () => {
    const playBtn = document.querySelector(".intro button");
    const introScreen = document.querySelector(".intro");
    const match = document.querySelector(".match");

    playBtn.addEventListener("click", () => {
      introScreen.classList.add("fadeOut");
      match.classList.add("fadeIn");
    });
  };
  //Play Match
  const playMatch = () => {
    const options = document.querySelectorAll(".options button");
    const playerHand = document.querySelector(".player-hand");
    const computerHand = document.querySelector(".computer-hand");
    const hands = document.querySelectorAll(".hands img");

    hands.forEach(hand => {
      hand.addEventListener("animationend", function() {
        this.style.animation = "";
      });
    });
    //Computer Options
    const computerOptions = ["rock", "paper", "scissors"];

    options.forEach(option => {
      option.addEventListener("click", function() {
        //Computer Choice
        const computerNumber = Math.floor(Math.random() * 3);
        const computerChoice = computerOptions[computerNumber];

        setTimeout(() => {
          //Here is where we call compare hands
          compareHands(this.textContent, computerChoice);
          //Update Images
          playerHand.src = `./assets/${this.textContent}.png`;
          computerHand.src = `./assets/${computerChoice}.png`;
        }, 2000);
        //Animation
        playerHand.style.animation = "shakePlayer 2s ease";
        computerHand.style.animation = "shakeComputer 2s ease";
      });
    });
  };
  
  const uptadeScore = () => {
    const playerScore = document.querySelector('.player-score p')
    const computerScore = document.querySelector('.computer-score p')
    playerScore.textContent = pScore
    computerScore.textContent = cScore
  }
  
  const compareHands = (playerChoie, computerChoice) => {
    const winner = document.querySelector('.winner')
    
    if(playerChoie === computerChoice){
      winner.textContent = 'It is a tie'
      return
    }
    // rock
    if(playerChoie === 'rock'){
      if(computerChoice === 'scissors'){
        winner.textContent = 'Player Wins'
        pScore++
        uptadeScore()
        return
      } else{
        winner.textContent = 'Computer Wins'
        cScore++
        uptadeScore()
        return
      }
    }
    // paper
    if(playerChoie === 'paper'){
      if(computerChoice === 'scissors'){
        winner.textContent = 'Computer Wins'
        cScore++
        uptadeScore()
        return
      } else{
        winner.textContent = 'Player Wins'
        pScore++
        uptadeScore()
        return
      }
    }
    // scissors
    if(playerChoie === 'scissors'){
      if(computerChoice === 'rock'){
        winner.textContent = 'Computer Wins'
        cScore++
        uptadeScore()
        return
      } else{
        winner.textContent = 'Player Wins'
        pScore++
        uptadeScore()
        return
      }
    }
  }
  
  startGame();
  playMatch();
}
game();